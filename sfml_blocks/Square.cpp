#include <cmath>
#include "Square.hpp"
#include "Screen.hpp"

using namespace std;

void Square::draw(Screen& screen, int y) const{
    sf::RectangleShape result({side, side});
    result.setFillColor(color);
    result.setOutlineColor(sf::Color::Black);
    result.setOutlineThickness(-0.5f);
    result.move(x*side, y*side);
    screen.draw(result);
}

