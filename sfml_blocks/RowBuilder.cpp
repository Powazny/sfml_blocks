#include "RowBuilder.hpp"
#include "Random.hpp"

std::vector<int> getRandomSquareIndexes(int numOfSquares, int min, int max){
    std::vector<int> squares(numOfSquares);
    max = std::min<int>(max, squares.size());
    std::iota(squares.begin(), squares.end(), 0);
    std::random_shuffle(squares.begin(), squares.end());

    int numberOfSquares = Random::getBetween(min, max);
    return std::vector<int>(squares.begin(), squares.begin() + numberOfSquares);
}


RowBuilder::RowBuilder(size_t numberOfColumns, SquaresBuilder& squaresBuilder):
    numberOfColumns(numberOfColumns),
    squaresBuilder(squaresBuilder)
{}

Row RowBuilder::build(){
    return Row(numberOfColumns, getRandomSquareIndexes(numberOfColumns, 1, numberOfColumns - 1), squaresBuilder);
}

Row RowBuilder::build(const std::vector<int>& squaresColumns){
    return Row(numberOfColumns, squaresColumns, squaresBuilder);
}
