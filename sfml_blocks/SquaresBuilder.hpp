#pragma once

#include "SFML/Graphics/Color.hpp"

#include "Square.hpp"

sf::Color randomColor();

struct SquaresBuilder{
    SquaresBuilder(float side);
    Square build(int x) const;
private:
    const float side;
};
