#pragma once

#include <vector>
#include <memory>

#include "Row.hpp"
#include "RowBuilder.hpp"

struct Screen;

std::vector<int> getRandomSquareIndexes(int numOfSquares, int min, int max);

struct Board{
    Board(size_t numOfRows, size_t fillRows, RowBuilder& rowBuilder);
    void addSquareBelow(int column);
    void addRowAtTheTop();
    void draw(Screen& screen);
    size_t getSize();

private:
    size_t numOfRows;
    size_t numOfColumns;
    RowBuilder& rowBuilder;
    std::vector<Row> rows;
};
