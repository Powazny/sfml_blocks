#pragma once

#include <cmath>
#include <SFML/Graphics.hpp>

#include "Random.hpp"

struct Screen;

using namespace std;

struct Square{
    Square(float side, int x, sf::Color color):
        x(x),
        side(side),
        color(color)
    {
    }
    void draw(Screen& screen, int height) const;
    const int x;
private:
    const float side;
    const sf::Color color;
};
