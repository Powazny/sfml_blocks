#pragma once

#include <random>

struct Random{
    static int getBetween(int from, int to);
private:
    static std::random_device rd;
    static std::mt19937 mt;
};
